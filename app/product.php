<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['name', 'discount', 'shortDetails', 'description', 'stock', 'new', 'sale', 'id_subcategory', 'tela', 'tecnica', 'id_collection'];

    protected function pictures()
    {
        return $this->hasMany(pictures::class, 'id_product');
    }

    protected function sizes()
    {
        return $this->hasMany(pivot_size::class, 'id_product');
    }

    protected function prices()
    {
        return $this->hasMany(pivot_price::class, 'id_product');
    }

    protected function sub_category()
    {
        return $this->belongsTo(sub_category::class, 'id_subcategory', 'id');
    }
    protected function collection(){
        return $this->belongsTo(collection::class, 'id_collection','id');
    }
}
