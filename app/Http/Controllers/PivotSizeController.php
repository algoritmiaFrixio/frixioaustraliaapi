<?php

namespace App\Http\Controllers;

use App\pivot_size;
use Illuminate\Http\Request;

class PivotSizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pivot_size  $pivot_size
     * @return \Illuminate\Http\Response
     */
    public function show(pivot_size $pivot_size)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pivot_size  $pivot_size
     * @return \Illuminate\Http\Response
     */
    public function edit(pivot_size $pivot_size)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pivot_size  $pivot_size
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pivot_size $pivot_size)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pivot_size  $pivot_size
     * @return \Illuminate\Http\Response
     */
    public function destroy(pivot_size $pivot_size)
    {
        //
    }
}
