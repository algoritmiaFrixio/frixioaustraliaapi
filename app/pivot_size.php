<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pivot_size extends Model
{
    protected function sizes()
    {
        return $this->belongsTo(size::class, 'id_size', 'id');
    }
}
