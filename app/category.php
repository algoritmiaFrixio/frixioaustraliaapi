<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected function sub_categories()
    {
        return $this->hasMany(sub_category::class, 'id_category');
    }
}
