<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivot_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_size');
            $table->unsignedInteger('id_product');
            $table->timestamps();

            $table->foreign('id_size')->references('id')->on('sizes');
            $table->foreign('id_product')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivot_sizes');
    }
}
