<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CollectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('collections')->insert([
            'name' => 'Hecho a mano',
            'img' => 'images/collection/hechoamano/Hecho-a-mano-Caballero.png'
        ]);
        DB::table('collections')->insert([
            'name' => 'Origen',
            'img' => 'images/collection/origen/Origen-Hombre.png'
        ]);
        DB::table('collections')->insert([
            'name' => 'Hilando Futuro',
            'img' => 'images/collection/hilandofuturo/HilandomiFuturo.png'
        ]);
    }
}
